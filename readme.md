# Snowscape, a CiviCRM sub-theme

![Theme screenshot](snowscape/screenshot.png)


## Package for upload

1. Install: `make sassc`

2. In the top-level directory, run: `make`


## License

Snowscape for CiviCRM is a sub-theme of [Olivero](https://www.drupal.org/project/olivero), the Drupal default theme as of version 10. A previous version was based on [Bartik](https://www.drupal.org/project/bartik), the default theme for Drupal 9. Drupal is released under the [GNU GPL 2.0 License](https://git.drupalcode.org/project/drupal/-/blob/11.x/core/LICENSE.txt). With any exception(s) noted below, this theme is released under the MIT license.

The [Nunito](https://github.com/vernnobile/NunitoFont) font family is licensed under the [SIL Open Font License](https://github.com/vernnobile/NunitoFont/blob/master/OFL.txt).
