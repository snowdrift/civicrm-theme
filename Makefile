version := $(shell cat snowscape/snowscape.info.yml | grep "version:" | \
	tr -d "version: ")

.PHONY: build
build:
	rm -rf *.tar.gz
	sassc -t compressed base-sass/_base.sass > snowscape/css/base.css
	tar cf "snowscape-$(version).tar.gz" snowscape

.PHONY: clean
clean:
	rm -rf *.tar.gz
